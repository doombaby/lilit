#!/bin/bash
# Create a Pulse Null Sink
create_sink () {
  pactl load-module module-null-sink media.class=Audio/Sink sink_name=lil-sink channel_map=stereo
  sleep 1
}

create_sink

# Start Audio Mixer
check_non () {
    ps -C non-mixer -o pid=
}

start_mixer () {
    check_non
    status=$?
    if [ $status -eq 1  ]; then
	non-mixer --instance lilit /home/jc/.non-mixer/lilit &
	sleep 2
    fi
} 

start_mixer

# Connect Mixer to Pulse Null Sink
connect_sink () {
  pw-link lil-sink:monitor_FL lilit/Chromium:in-1
  pw-link lil-sink:monitor_FR lilit/Chromium:in-2 
  pw-link lilit/Chromium:out-1 alsa_output.pci-0000_00_1f.3.analog-stereo:playback_FL 
  pw-link lilit/Chromium:out-2 alsa_output.pci-0000_00_1f.3.analog-stereo:playback_FR 
}

connect_sink
