#! /bin/bash

# Download new multilib packages

download_multilib () {
    echo -e "Syncing alienbob multilib repository\n"
    cd $HOME/slackbuild/multilib/
    lftp -c 'open http://slackware.com/~alien/multilib/ ; mirror --only-newer -c -e current'    
}

download_multilib

# Upgrade libraries

upgrade_libraries () {
    echo -e "Upgrading slackware multilib libraries\n"
    cd $HOME/slackbuild/multilib/current
    sudo upgradepkg --install-new *.t?z    
}

upgrade_libraries

# Upgrade packages

upgrade_packages () {
    echo -e "Upgrading slackware multilib packages\n"    
    cd $HOME/slackbuild/multilib/current
    sudo upgradepkg --install-new slackware64-compat32/*-compat32/*.t?z    
}

upgrade_packages
