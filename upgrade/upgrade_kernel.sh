#!/bin/bash

# Kernel Upgrade Script

version=$(curl https://slackware.osuosl.org/slackware64-current/kernels/VERSIONS.TXT | grep -oP '(?<=version ).*' | sed -r 's/\.$//')
partition1="/dev/nvme0n1p1"
partition2="/dev/nvme0n1p2"
initramfs="initrd-$version.gz"
rootfs="/dev/cryptvg/root"
blacklist="/etc/slackpkg/blacklist"
filesystem="xfs"
bootdir="/boot"
slackbuild="$HOME/slackbuild/kernel"
kernel_pkgs="kernel-generic kernel-huge kernel-modules kernel-headers kernel-source"

# Backup Blacklist
backup_blacklist () {
    sudo cp $blacklist $blacklist.$(uname -r)
    sudo rm $blacklist
    sudo touch $blacklist
}

backup_blacklist

# Update Slackpkg
slackpkg_update () {
    sudo slackpkg update
}

slackpkg_update

# Install Kernel
install_kernel () {
    sudo slackpkg upgrade $(echo "$kernel_pkgs")   
}

install_kernel

# Restore Blacklist
restore_blacklist () {
    sudo cp $blacklist.$(uname -r) $blacklist
}

restore_blacklist

# Build initramfs
build_initramfs () {
    sudo /sbin/mkinitrd -c -k $version -m $filesystem -r $rootfs -C $partition2 -L -o $bootdir/$initramfs
}

build_initramfs

# Copy kernel to slackbuild
copy_slackbuild () {
    if [ ! -d $slackbuild/$version ]; then
	mkdir $slackbuild/$version
    fi
    cd $slackbuild/$version
    cp $bootdir/$initramfs .
    cp $bootdir/vmlinuz-generic-$version .
    cp $bootdir/System.map-generic-$version .
    cp $bootdir/config-generic-$version.x64 .    
}

copy_slackbuild

# Mount boot partition
mount_boot () {
    sudo mount $partition1 $bootdir
}

mount_boot

# Copy kernel to boot
copy_boot () {
    sudo cp $slackbuild/$version/$initramfs $bootdir/EFI/Slackware/
    sudo cp $slackbuild/$version/vmlinuz-huge-$version $bootdir/EFI/Slackware/
}

copy_boot

# Generate elilo
generate_elilo () {
    sudo echo -e '#prompt \n
chooser=simple \n
delay=10 \n
timeout=10 \n
# \n 
image=vmlinuz-generic-$version \n
        label=slackware \n
        initrd=initrd-$version.gz \n
        read-only \n
        append="root=/dev/cryptvg/root ro"' > $bootdir/EFI/Slackware/elilo.conf
}

generate_elilo
