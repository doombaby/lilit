#!/bin/bash
# SLACKPKG EXIT STATUS
#   0    Successful slackpkg execution.
#
#   1    Something wrong happened.
#
#   20   No package found to be downloaded, installed, reinstalled, upgraded, or removed.
#
#   50   Slackpkg itself was upgraded and you need to re‐run it.
#
#   100  There are pending updates.
slackpkg_log=$HOME/slackbuild/log.txt
log_date=$(date  +%c)
log_mesg=${log_mesg:-"null"}

lil_ipv4() {
    interface="${1:-wlan0}"
    ip -j address show $interface | jq -r '.[].addr_info[] | select(.family=="inet") | .local'
}

log_f () {
    echo $log_mesg >> $slackpkg_log
}

start_mirror() {
    if [ $(lil_ipv4) == "10.0.10.17" ]; then
	ssh kodaiji "setsid mirror_slackware.sh start > /dev/null 2>&1 < /dev/null &"
    fi    
}

stop_mirror() {
    if [ $(lil_ipv4) == "10.0.10.17" ]; then
	ssh kodaiji "setsid mirror_slackware.sh stop > /dev/null 2>&1 < /dev/null &"
    fi        
}

update_slackware () {
    slackpkg check-updates
    status=$?
    if [ $status -eq 0 ]; then
	log_mesg="$log_date No packages found to be downloaded, installed reinstalled, upgraded, or removed."
	log_f
	exit
    elif [ $status -eq 50 ]; then
	sudo slackpkg update
	sudo slackpkg upgrade slackpkg
	log_mesg="$log_date Slackpkg has been upgraded."
	log_f
	sudo slackpkg install-new
	sudo slackpkg upgrade-all
	log_mesg="$log_date All packages have been upgraded."
	log_f
    elif [ $status -eq 100 ]; then
	sudo slackpkg update
	sudo slackpkg install-new
	sudo slackpkg upgrade-all
	log_mesg="$log_date All packages have been upgraded."
	log_f
    elif [ $status -eq 1 ]; then
	log_mesg="$log_date Something went wrong."
	log_f
    fi
}

#start_mirror

update_slackware

#stop_mirror
